import Page from '../classes/Page';

const mainPage = new Page({
  onCreate() {
    console.log('mainpage create')
  },
  onInit() {
    const colapseBtns = document.getElementsByClassName('collapseBtn')

    for (let btn of colapseBtns) {
      btn.classList.add('collapsed')
    }
    console.log('mainpage init')
    console.log(this)
    const sliderWrap = document.getElementById('slider')
    const controls = document.getElementById('controls')
    const left = document.getElementById('left')
    const right = document.getElementById('right')

    // if (sliderWrap) {
    //   const slider = tns({
    //     container: '.my-slider',
    //     items: 1,
    //     "arrowKeys": true,
    //     "slideBy": 1,
    //     touch: true,
    //     nav: false,
    //     "fixedWidth": false,
    //     controlsPosition: 'bottom',
    //     gutter: 25,
    //     controlsText: ['', ''],
    //     controlsContainer: controls,
    //     prevButton: left,
    //     nextButton: right,
    //     preventActionWhenRunning: true,
    //     lazyload: true,
    //     "mouseDrag": true,
    //     "swipeAngle": 15,
    //     "responsive": {
    //       "1000": {
    //         "fixedWidth": 440,
    //         "items": 2,
    //       },
    //     },
    //   });
    // }
  },
  onDestroy() {
    console.log('mainpage destroy')
  },
  name: 'mainpage',
  rootElementId: 'js-page-mainpage'
});

export default mainPage;
