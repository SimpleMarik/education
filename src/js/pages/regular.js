import Page from '../classes/Page';
import Swiper, { Navigation } from 'swiper';
Swiper.use([Navigation]);

const regularPage = new Page({
  onCreate() {
    console.log('regular create')
  },
  onInit() {
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: "auto",
      spaceBetween: 20,
      loop: true,
    });
    var swiper = new Swiper(".mainSwiper", {
      slidesPerView: "auto",
      spaceBetween: 20,
      navigation: {
        nextEl: ".left",
        prevEl: ".right",
      },
      loop: true,
    });
    var tel = document.getElementById('tel');
    var maskOptions = {
      mask: '+{7} (000) 000-00-00'
    };
    if (tel) {
      const mask = IMask(tel, maskOptions);
    }
    var modaltel = document.getElementById('modalTel');
    if (modaltel) {
      const mask = IMask(modaltel, maskOptions);
    }
    const colapseBtns = document.getElementsByClassName('collapseBtn')

    for (let btn of colapseBtns) {
      btn.classList.add('collapsed')
    }
    const coursesButtons = document.getElementsByClassName('courses__filter-btn')
    if (coursesButtons) {
      for (const btn of coursesButtons) {
        btn.onclick = function () {
          for (const btn of coursesButtons) {
            btn.classList.remove('active')
          }
          this.classList.add('active')
        }
      }
    }
  },
  onDestroy() {
    console.log('mainpage destroy')
  },
  name: 'regular',
  rootElementId: 'js-page-regular'
});

export default regularPage;
