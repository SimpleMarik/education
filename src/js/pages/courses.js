import Page from '../classes/Page';
import {noop} from "bootstrap/js/src/util";

const mainPage = new Page({
  onCreate() {
    console.log('courses create')

    const coursesButtons = document.getElementsByClassName('courses__filter-btn')
    for (const btn of coursesButtons) {
      btn.onclick = function () {
        for (const btn of coursesButtons) {
          btn.classList.remove('active')
        }
        this.classList.add('active')
      }
    }

    const coursesButtonsMobile = document.getElementsByClassName('courses__filter-btn-mobile')
    for (const btn of coursesButtonsMobile) {
      btn.onclick = function () {
        for (const btn of coursesButtonsMobile) {
          btn.classList.remove('active')
        }
        this.classList.add('active')
      }
    }


    const sliderWrap = document.getElementById('coursesSlider')
    if (sliderWrap) {
      const slider = tns({
        container: '.courses__slider',
        items: 2,
        "arrowKeys": true,
        "slideBy": 1,
        touch: true,
        nav: false,
        controls: false,
        "mouseDrag": true,
        "swipeAngle": 15,
        "responsive": {
          "1000": {
            "items": 2,
          },
        },
      });
    }
  },
  onInit() {
    console.log('courses create')

  },
  onDestroy() {
    console.log('courses destroy')
  },
  name: 'mainpage',
  rootElementId: 'js-page-mainpage'
});

export default mainPage;
